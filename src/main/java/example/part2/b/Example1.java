package example.part2.b;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.stream.Collectors.toMap;

public class Example1 {

    public static void main(String[] args) {
        List<String> list = newArrayList("1", "2", "3");

        //foreach
        list.stream().forEach(System.out::println);
        //

        //map
        Stream<Integer> integerStream = list.stream().map(Integer::parseInt);
        IntStream intStream = list.stream().mapToInt(i -> Integer.parseInt(i)); //IntStream.range(1, 10);
        List<Integer> intList = integerStream.collect(Collectors.toList());
        //

        //flatMap
        List<ArrayList<String>> arrayLists = list.stream().map(e -> newArrayList(e)).collect(Collectors.toList());
        List<String> flattedList = list.stream().flatMap(e -> newArrayList(e).stream()).collect(Collectors.toList());
        System.out.println("flattedList=" + flattedList);
        //

        //filter
        List<Integer> filtered = list.stream().map(Integer::parseInt).filter(e -> e < 3).collect(Collectors.toList());
        System.out.println("filtered=" + filtered);
        //

        //reduce
        String reduced = list.stream().reduce((x, y) -> x + y).get();
        System.out.println(reduced);
        //

        //toSet
        Set<String> set = list.stream().collect(Collectors.toSet());
        System.out.println("set" + set);
        //

        //toMap
        Map<String, Integer> map = list.stream().collect(toMap(e -> e, e -> e.length()));
        System.out.println("map" + map);
        //

    }

}
