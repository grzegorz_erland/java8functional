package example.part2.b;

import java.util.NoSuchElementException;
import java.util.Optional;

public class Example4 {

    public static void main(String[] args) {

        //
        Optional<String> stringOptional = Optional.of("abc");
        System.out.println(stringOptional);
        System.out.println(stringOptional.isPresent());
        System.out.println(stringOptional.get());
        //

        //OptionalLong/Int/Double

        //
        Optional<String> stringOptional2 = Optional.ofNullable(null);
        System.out.println(stringOptional2);
        System.out.println(stringOptional2.isPresent());
        try {
            System.out.println(stringOptional2.get());
        } catch (NoSuchElementException ex) {
            System.out.println("Could not get value from second optional");
        }
        System.out.println(stringOptional2.orElse("Default value"));
        //

        //
        try {
            Optional.of(null);
        } catch (NullPointerException ex) {
            System.out.println("Can not create from null value");
        }
        //

        //
        Optional<String> stringOptional3 = Optional.of("ke").map(Example4::appendKe);
        System.out.println(stringOptional3);
        Optional<String> stringOptional4 = Optional.ofNullable((String) null).map(Example4::appendKe);
        System.out.println(stringOptional4);
        //

        //
        Optional<String> person = Optional.of("Person");
        Optional<String> bankAccount = Optional.of("Bank account");
        Optional<Integer> amount = Optional.of(100);

        Optional<Integer> presentAmount = person.flatMap(p -> bankAccount).flatMap(ba -> amount);
        System.out.println("presentAmount=" + presentAmount);

        Optional<String> noBankAccount = Optional.empty();
        Optional<Integer> emptyAmount = person.flatMap(p -> noBankAccount).flatMap(ba -> amount);
        System.out.println("emptyAmount=" + emptyAmount);
        //

    }

    public static String appendKe(String value) {
        return value + "ke";
    }

}
