package example.part2.b;

import java.util.stream.IntStream;

public class Example3 {

    public static void main(String[] args) {
        IntStream.range(0, 10)
                .forEach(System.out::println);

        IntStream.iterate(0, (x) -> x + 1)
                .map(i -> i * i)
                .forEach(i -> {
                    System.out.println(i);
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                });
    }

}
