package example.part2.b;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.google.common.collect.Lists.newArrayList;

public class Example2 {

    public static void main(String[] args) {
        List<String> list = newArrayList("1", "2", "3");

        List<String> list2 = list.stream()
                .map(Example2::verboseParseInt)
                .map(Example2::verboseToString)
                .collect(Collectors.toList());

        Stream<String> stringStream = list.stream()
                .map(Example2::verboseParseInt)
                .map(Example2::verboseToString);

        System.out.println("BEFORE");
        Stream<Integer> integerStream = mapToInt(list);
        System.out.println("IN BETWEEN");
        Stream<String> stringStream2 = mapToString(integerStream);
        System.out.println("AFTER");
        stringStream2.collect(Collectors.toList());
    }

    public static Integer verboseParseInt(String element) {
        System.out.println("toInt=" + element);
        return Integer.parseInt(element);
    }

    public static String verboseToString(Integer element) {
        System.out.println("toString=" + element);
        return Integer.toString(element);
    }

    public static Stream<Integer> mapToInt(List<String> list) {
        return list.stream().map(Example2::verboseParseInt);
    }

    public static Stream<String> mapToString(Stream<Integer> stream) {
        return stream.map(Example2::verboseToString);
    }

}
