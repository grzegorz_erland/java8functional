package example.part2.c;

import java.util.stream.IntStream;

public class Example {

    public static void main(String[] args) {

        long time = System.currentTimeMillis();
        IntStream.range(1, 1_000_000_000)
                .filter(Example::divisibleBy13).count();
        time = System.currentTimeMillis() - time;

        System.out.println("seq=" + time);

        time = System.currentTimeMillis();
        IntStream.range(1, 1_000_000_000)
                .parallel()
                .filter(Example::divisibleBy13)
                .count();
        time = System.currentTimeMillis() - time;
        //Lists.newArrayList(1, 2, 3).parallelStream();

        System.out.println("par=" + time);

//        IntStream.range(1, 1_000_000_000)
//                .parallel()
//                .filter(Example::divisibleBy13)
//                .sequential()
//                .limit(20)
//                .forEach(System.out::println);

        time = System.currentTimeMillis();
        IntStream.range(1, 1_000_000_000)
                .parallel()
                .filter(Example::divisibleBy13)
                .sequential()
                .count();
        time = System.currentTimeMillis() - time;

        System.out.println("parToSeq=" + time);


        //Arrays.parallelSort(new String[]{"a", "b", "c"});
        //If the length of the specified array is less than the minimum granularity, then it is sorted using the appropriate Arrays.sort method.
    }

    public static boolean divisibleBy13(int value) {
        return value % 13 == 0;
    }

}
