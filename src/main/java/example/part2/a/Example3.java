package example.part2.a;

import java.util.function.IntConsumer;
import java.util.function.IntFunction;
import java.util.function.IntSupplier;

public class Example3 {

    public static void main(String[] args) {

        //
        printInt(Example3::staticMethod);
        printInt(new IntContainer(3)::getValue);
        //

        //
        IntFunction intContainerConstructor = IntContainer::new;

        System.out.println(intContainerConstructor);
        System.out.println(intContainerConstructor.apply(4));

        IntConsumer intConsumer = IntContainer::new; //can't create objects from such interface
        //

    }

    public static void printInt(IntSupplier intSupplier) {
        System.out.println(intSupplier.getAsInt());
    }

    public static int staticMethod() {
        return 1;
    };

    public static class IntContainer {
        private int value;

        public int getValue() {
            return value;
        }

        public IntContainer(int value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return "IntContainer{" +
                    "value=" + value +
                    '}';
        }
    }

}
