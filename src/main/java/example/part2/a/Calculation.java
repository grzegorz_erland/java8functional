package example.part2.a;

@FunctionalInterface
public interface Calculation {

    int calculate(int x, int y);

}
