package example.part2.a;

import java.util.concurrent.Callable;
import java.util.function.DoubleToLongFunction;

/*
java.util.function

1 	BiConsumer<X, Y>            (X ,Y) -> void
2 	BiFunction<X, Y, Z>         (X, Y) -> Z
3 	BinaryOperator<X>           (X) -> X
4 	BiPredicate<X, Y>           (X, Y) -> boolean
5 	BooleanSupplier             () -> boolean
6 	Consumer<X>                 (X) -> void

7 	DoubleBinaryOperator        (double, double) -> double
8 	DoubleConsumer              (double) -> void
9 	DoubleFunction<X>           (double) -> X
10 	DoublePredicate             (double) -> boolean
11 	DoubleSupplier              () -> double
12 	DoubleToIntFunction         (double) -> int
13 	DoubleToLongFunction        (double) -> long
14 	DoubleUnaryOperator         (double) -> double

15 	Function<X, Y>              (X) -> Y

16 	IntBinaryOperator           (int, int) -> int
17 	IntConsumer                 (int) -> void
18 	IntFunction<X>              (int) -> X
19 	IntPredicate                (int) -> boolean
20 	IntSupplier                 () -> int
21 	IntToDoubleFunction         (int) -> double
22 	IntToLongFunction           (int) -> long
23 	IntUnaryOperator            (int) -> int

24 	LongBinaryOperator          (long, long) -> long
25 	LongConsumer                (long) -> void
26 	LongFunction<X>             (long) -> X
27 	LongPredicate               (long) -> boolean
28 	LongSupplier                () -> long
29 	LongToDoubleFunction        (long) -> double
30 	LongToIntFunction           (long) -> int
31 	LongUnaryOperator           (long) -> long

32 	ObjDoubleConsumer<X>        (X, double) -> void
33 	ObjIntConsumer<X>           (X, int) -> void
34 	ObjLongConsumer<X>          (X, long) -> void

35 	Predicate<X>                (X) -> boolean
36 	Supplier<X>                 () -> X

37 	ToDoubleBiFunction<X, Y>    (X, Y) -> double
38 	ToDoubleFunction<X>         (X) -> double

39 	ToIntBiFunction<X, Y>       (X, Y) -> int
40 	ToIntFunction<X>            (X) -> int

41 	ToLongBiFunction<X, Y>      (X, Y) -> long
42 	ToLongFunction<X>           (X) -> long

43 	UnaryOperator<X>            (X) -> X
 */
public class Example2 {

    public static void main(String[] args) {
        long result = toLong(1, Double::doubleToLongBits);
        System.out.println(result);

        perform(() -> System.out.println("runnable action"));

        System.out.println(getResult(() -> 10));

//        perform(() -> {
//            throw new IllegalStateException("Can't catch this!");
//        });
    }

    public static long toLong(double value, DoubleToLongFunction function) {
        return function.applyAsLong(value);
    }

    public static void perform(Runnable action) {
        action.run();
    }

    public static Integer getResult(Callable<Integer> supplier) {
        try {
            return supplier.call();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
