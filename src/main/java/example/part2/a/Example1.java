package example.part2.a;

public class Example1 {

    public static void main(String[] args) {
        int result1 = performCalculation(10, 5, new Addition());
        int result2 = performCalculation(10, 5, new Multiplication());

        System.out.println("addition=" + result1);
        System.out.println("multiplication=" + result2);


        int result3 = performCalculation(10, 5, new Calculation() {
            @Override
            public int calculate(int x, int y) {
                return x / y;
            }
        });

        System.out.println("division=" + result3);

        int result4 = performCalculation(10, 5 , (int x, int y) -> {
            return (int)Math.pow(x, y);
        });
        int result5 = performCalculation(10, 5 , (x, y) -> (int)Math.pow(x, y));

        System.out.println("power1=" + result4);
        System.out.println("power2=" + result5);

    }

    public static int performCalculation(int x, int y, Calculation calculation) {
        return calculation.calculate(x, y);
    }

    public static class Addition implements Calculation {
        @Override
        public int calculate(int x, int y) {
            return x + y;
        }
    }

    public static class Multiplication implements Calculation {
        @Override
        public int calculate(int x, int y) {
            return x + y;
        }
    }

}
