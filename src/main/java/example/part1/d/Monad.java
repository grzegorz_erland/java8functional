package example.part1.d;

import java.util.function.Function;

public interface Monad<X> {

    X get();

    <Y> Monad<Y> unit(Y value);

    <Y> Monad<Y> map(Function<X, Y> function);

    <Y> Monad<Y> flatMap(Function<X, Monad<Y>> function);

}
