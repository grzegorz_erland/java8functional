package example.part1.d;

import java.util.function.Function;

import static java.lang.String.format;

public class ExceptionHandlingMonad<X> implements Monad<X> {

    private X value;
    private String exceptionMessage;

    private ExceptionHandlingMonad(X value, String exceptionMessage) {
        this.value = value;
        this.exceptionMessage = exceptionMessage;
    }

    public static <X> ExceptionHandlingMonad ofValue(X value) {
        return new ExceptionHandlingMonad<>(value, null);
    };

    public static <X> ExceptionHandlingMonad withException(String exceptionMessage) {
        return new ExceptionHandlingMonad<>(null, exceptionMessage);
    };

    @Override
    public X get() {
        if (hasException()) {
            throw new IllegalStateException(format("Has no result due to exception: '%s'", exceptionMessage));
        }
        return value;
    }

    public boolean hasException() {
        return exceptionMessage != null;
    }

    @Override
    public <Y> Monad<Y> unit(Y value) {
        return ofValue(value);
    }

    @Override
    public <Y> Monad<Y> map(Function<X, Y> function) {
        if (hasException()) {
            return withException(exceptionMessage);
        }
        try {
            Y yValue = function.apply(value);
            Monad<Y> yMonad = unit(yValue);
            return yMonad;
        } catch (Exception ex) {
            return withException(ex.getMessage());
        }
    }

    @Override
    public <Y> Monad<Y> flatMap(Function<X, Monad<Y>> function) {
        if (hasException()) {
            return withException(exceptionMessage);
        }
        try {
            return function.apply(value);
        } catch (Exception ex) {
            return withException(ex.getMessage());
        }
    }

    @Override
    public String toString() {
        if(hasException()) {
            return format("exceptionMessage='%s'", exceptionMessage);
        } else {
            return format("value=%s", value);
        }
    }

}
