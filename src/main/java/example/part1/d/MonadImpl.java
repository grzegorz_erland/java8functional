package example.part1.d;

import java.util.function.Function;

public class MonadImpl<X> implements Monad<X> {

    private final X value;

    public MonadImpl(X value) {
        this.value = value;
    }

    @Override
    public X get() {
        return null;
    }

    @Override
    public <Y> Monad<Y> unit(Y value) {
        return new MonadImpl<>(value);
    }

    @Override
    public <Y> Monad<Y> map(Function<X, Y> function) {
        Y yValue = function.apply(value);
        Monad<Y> yMonad = unit(yValue);
        return yMonad;
    }

    @Override
    public <Y> Monad<Y> flatMap(Function<X, Monad<Y>> function) {
        return function.apply(value);
    }

    @Override
    public String toString() {
        return "MonadImpl{" +
                "value=" + value +
                '}';
    }
}
