package example.part1.d;

public class Example2 {

    public static void main(String[] args) {
        
        Monad<Integer> monad1 = ExceptionHandlingMonad.ofValue(20);
        Monad<Integer> result1 = monad1.map(x -> divide(x, 5)).map(x -> divide(x ,2));

        System.out.println(result1);

        Monad<Integer> monad2 = ExceptionHandlingMonad.ofValue(1);
        Monad<Integer> result2 = monad2.map(x -> divide(x, 0)).map(x -> divide(x, 1)).map(x -> divide(x, 1));

        System.out.println(result2);

    }

    public static int divide(int x, int y) {
        return x / y;
    }
    
}
