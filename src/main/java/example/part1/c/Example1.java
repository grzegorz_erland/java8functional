package example.part1.c;

public class Example1 {

    public static void main(String[] args) {
        int result1 = divide(divide(20, 5), 2);

        System.out.println(result1);

        int result2 = divide(1, 0);

        System.out.println(result2);
    }

    public static int divide(int x, int y) {
        return x / y;
    }


}
