package example.part1.c;

public class ResultWithState {

    private final int result;
    private final int state;

    public ResultWithState(int result, int state) {
        this.result = result;
        this.state = state;
    }

    public int getResult() {
        return result;
    }

    public int getState() {
        return state;
    }

    @Override
    public String toString() {
        return "result=" + result + ", state=" + state;
    }
}
