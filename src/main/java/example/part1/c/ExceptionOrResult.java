package example.part1.c;

import static java.lang.String.format;

public class ExceptionOrResult {

    private int result;
    private String exceptionMessage;

    public ExceptionOrResult(String exceptionMessage) {
        this.exceptionMessage = exceptionMessage;
    }

    public ExceptionOrResult(int result) {
        this.result = result;
    }

    public boolean hasException() {
        return exceptionMessage != null;
    }

    public int getResult() {
        if (hasException()) {
            throw new IllegalStateException(format("Has no result due to exception: '%s'", exceptionMessage));
        }
        return result;
    }

    public String getExceptionMessage() {
        return exceptionMessage;
    }

    @Override
    public String toString() {
        if(hasException()) {
            return format("exceptionMessage='%s'", exceptionMessage);
        } else {
            return format("result=%s", result);
        }
    }
}
