package example.part1.c;

public class Example3 {

    public static void main(String[] args) {
        ResultWithState result1 = divide(divide(new ResultWithState(20, 0), new ResultWithState(5, 0)), new ResultWithState(2, 0));

        System.out.println(result1);

        ResultWithState result2 = divide(new ResultWithState(1, 0), new ResultWithState(1, 0));

        System.out.println(result2);
    }

    public static ResultWithState divide(ResultWithState x, ResultWithState y) {
        int result = x.getResult() / y.getResult();
        int state = x.getState() + y.getState() + 1;
        return new ResultWithState(result, state);
    }

}
