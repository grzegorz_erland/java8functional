package example.part1.c;

public class Example2 {

    public static void main(String[] args) {
        ExceptionOrResult result1 = divide(divide(new ExceptionOrResult(20), new ExceptionOrResult(5)), new ExceptionOrResult(2));

        System.out.println(result1);

        ExceptionOrResult result2 = divide(new ExceptionOrResult(1), new ExceptionOrResult(0));

        System.out.println(result2);
    }

    public static ExceptionOrResult divide(ExceptionOrResult x, ExceptionOrResult y) {
        try {
            if (x.hasException()) {
                return new ExceptionOrResult(x.getExceptionMessage());
            }
            if (y.hasException()) {
                return new ExceptionOrResult(y.getExceptionMessage());
            }
            int result = x.getResult() / y.getResult();
            return new ExceptionOrResult(result);
        } catch (ArithmeticException ex) {
            return new ExceptionOrResult(ex.getMessage());
        }
    }

}
