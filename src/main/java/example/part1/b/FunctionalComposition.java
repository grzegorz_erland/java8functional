package example.part1.b;

import java.util.function.Function;

public interface FunctionalComposition {

    /*
        append(f1, f2)(x) == f2(f1(x))
        append(f1, append(f2, f3))(x) == append((append(f1, f2)), f3)(x)
     */
    <X, Y, Z> Function<X, Z> append(Function<X, Y> f1, Function<Y, Z> f2);

    /*
        identity()(f(x)) == f(identity()(x)) == f(x)
     */
    <X> Function<X, X> identity();

}
