package example.part1.b;

import java.util.function.Function;

public class Example {

    public static void main(String[] args) {
        FunctionalCompositionImpl functionalComposition = new FunctionalCompositionImpl();

        Function<String, Integer> stringToInt = Integer::parseInt;
        Function<Integer, Double> intToDouble = Integer::doubleValue;

        Function<String, Double> stringToDouble = functionalComposition.append(stringToInt, intToDouble);

        System.out.println(stringToDouble.apply("10").getClass());

        Function<String, String> stringIdentity = functionalComposition.identity();

        System.out.println(stringIdentity.apply("10").getClass());
    }

}
