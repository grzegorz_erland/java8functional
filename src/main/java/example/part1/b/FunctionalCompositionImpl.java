package example.part1.b;

import java.util.function.Function;

public class FunctionalCompositionImpl implements FunctionalComposition {
    @Override
    public <X, Y, Z> Function<X, Z> append(Function<X, Y> f1, Function<Y, Z> f2) {
        return new Function<X, Z>() {
            @Override
            public Z apply(X x) {
                Y yValue = f1.apply(x);
                Z zValue = f2.apply(yValue);
                return zValue;
            }
        };
    }

    @Override
    public <X> Function<X, X> identity() {
        return new Function<X, X>() {
            @Override
            public X apply(X x) {
                return x;
            }
        };
    }
}
