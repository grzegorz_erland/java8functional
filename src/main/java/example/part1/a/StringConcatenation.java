package example.part1.a;

public class StringConcatenation implements Monoid<String> {

    /*
        "a" + ("b" + "c") == ("a" + "b") + "c" == "abc"
     */
    @Override
    public String append(String x, String y) {
        return x + y;
    }

    /*
        "a" + "" == "" + "a" == "a"
     */
    @Override
    public String indentity() {
        return "";
    }

}
