package example.part1.a;

/*
https://en.wikipedia.org/wiki/Monoid

In abstract algebra, a branch of mathematics, a monoid is an algebraic structure with a single associative binary operation and an identity element.
 */
public interface Monoid<X> {

    /*
        append(x, append(y, z)) == append(append(x, y), z)
     */
    X append(X x, X y);

    /*
        append(x, identity) == append(identity, x) == x
     */
    X indentity();

}
