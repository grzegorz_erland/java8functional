package example.part3.b;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.google.common.collect.Lists.newArrayList;

public class Example3 {

    public static void main(String[] args) {

    }

    private interface CurriedFunction extends Function<String, Function<String, Function<String, String>>> {}

    public static class Foo {
        private String a = "a";

        public List<String> createStringList(String b) {
            List<String> numbers = newArrayList("1", "2", "3");
            return numbers.stream().map(((CurriedFunction)x -> y -> z -> x + y + z).apply(a).apply(b)).collect(Collectors.toList());
        }

    }


}
