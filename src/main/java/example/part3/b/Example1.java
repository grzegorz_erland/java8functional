package example.part3.b;

public class Example1 {

    public static void main(String[] args) {
        new Bar().printExample();
    }

    public static class Bar {

        private String scopeExample = "Bar";

        public void printExample() {
            printThisScope(new Foo() {
                String scopeExample = "Foo";

                @Override
                public void print() {
                    System.out.println(this.scopeExample);
                }
            });
            printThisScope(() -> {
                String scopeExample = "Lambda";
                System.out.println(this.scopeExample);
            });
        }

        private void printThisScope(Foo foo) {
            foo.print();
        }
    }

}
