package example.part3.b;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import static com.google.common.collect.Lists.newArrayList;

public class Example2 {

    public static void main(String[] args) {
        Foo foo = new Foo();
        Foo2 foo2 = new Foo2();

        System.out.println(foo.createStringList("b"));
        System.out.println(foo2.createStringList(new BProvider()));
    }

    public static class Foo {
        private String a = "a";

        public List<String> createStringList(String b) {
            List<String> numbers = newArrayList("1", "2", "3");
            return numbers.stream().map(c -> a + b + c).collect(Collectors.toList());
        }

    }

    public static class Foo2 {
        private String a = "a";

        public List<String> createStringList(BProvider bProvider) {
            List<String> numbers = newArrayList("1", "2", "3");
            return numbers.stream().map(c -> a + bProvider.getB() + c).collect(Collectors.toList());
        }

    }

    public static class BProvider {

        private final Random random = new Random();

        private final List<String> possibleB = newArrayList("b", "B", "brb");

        public String getB() {
            return possibleB.get(random.nextInt(3));
        }

    }

}
