package example.part3.a;

import com.google.common.collect.Maps;

import java.util.Map;
import java.util.Optional;

public class Example1 {

    public static void main(String[] args) {
        Map<Integer, User> users = Maps.newHashMap();
        users.put(1, new User(new Address(new State())));

        User user = users.get(1);
        if(user != null) {
            Address address = user.getAddress();
            if(address != null) {
                State state = address.getState();
                if(state != null) {
                    printState(state);
                }
            }
        }

        Optional.ofNullable(users.get(1))
                .flatMap(User::getAddressOptional)
                .flatMap(Address::getStateOptional)
                .ifPresent(Example1::printState);
    }

    public static void printState(State state) {
        System.out.println(state);
    }

}
