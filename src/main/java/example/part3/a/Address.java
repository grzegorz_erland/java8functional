package example.part3.a;

import java.util.Optional;

public class Address {

    private final State state;
    private final Optional<State> stateOptional;

    public Address(State state) {
        this.state = state;
        this.stateOptional = Optional.ofNullable(state);
    }

    public State getState() {
        return state;
    }

    public Optional<State> getStateOptional() {
        return stateOptional;
    }

}
