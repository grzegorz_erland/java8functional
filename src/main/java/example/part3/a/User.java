package example.part3.a;

import java.util.Optional;

public class User {

    private final Address address;
    private final Optional<Address> addressOptional;

    public User(Address address) {
        this.address = address;
        this.addressOptional = Optional.ofNullable(address);
    }

    public Address getAddress() {
        return address;
    }

    public Optional<Address> getAddressOptional() {
        return addressOptional;
    }
}
